```php
setlocale(LC_ALL,'de_DE.utf8');
var_dump((float)"3,68");
// excepting 3.68 
// output: 3
// no error/notice/warning
var_dump((float)"3.68");
// output: 3.68
```

```php
setlocale(LC_ALL,'en_US.utf8');
var_dump((float)"3,68");
// excepting 3.68 
// output: 3
// no error/notice/warning
var_dump((float)"3.68");
// output: 3.68
```

```php
setlocale(LC_ALL,'de_DE.utf8');
var_dump((string)1.234);
//output: "1,234"
```

```php
setlocale(LC_ALL,'en_US.utf8');
var_dump((string)1.234);
//output: "1.234"
```

# Disscusion
- https://stackoverflow.com/questions/17587581/php-locale-dependent-float-to-string-cast
- https://bugs.php.net/bug.php?id=53711
