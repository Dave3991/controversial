```php
public function computeSafetyStock(int $skuId, int $skuProviderId): SafetyStockDataCollection
{
    ...
}
```

vs

```php
public function computeSafetyStock(Sku $sku, SkuProvider $skuProvider): SafetyStockDataCollection
{
    $skuId = $sku->getId();
    $skuProviderId = $skuProvider->getId();

    ...
}
```