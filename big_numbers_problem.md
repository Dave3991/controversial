```php
// php
$return = 9999999999999999998 + 1;
//excepted: 9999999999999999999
//output: 1.0E+19 #for 7.2.0 - 7.2.32, 7.3.0 - 7.3.20, 7.4.0 - 7.4.8, 8.0.0alpha1 - alpha3
```

```php
// php
$result = 1/3 * 3;
//excepted: 0.9999999999999999999999999999
//output: 1 #for 7.2.0 - 7.2.32, 7.3.0 - 7.3.20, 7.4.0 - 7.4.8, 8.0.0alpha1 - alpha3
```
```php
// php
var_dump(0.1 + 0.2 - 0.3);
Output for 8.0.0alpha1 - alpha3
float(5.551115123125783E-17)
Output for 5.5.0 - 5.5.38, 5.6.0 - 5.6.40, 7.0.0 - 7.0.33, 7.1.0 - 7.1.33, 7.2.0 - 7.2.32, 7.3.0 - 7.3.20, 7.4.0 - 7.4.8
float(5.5511151231258E-17)
```

```javascript
// javascript
var num = 5.9549999999999996;
num.toFixed(2)
"5.95"
var num = 5.9549999999999997;
num.toFixed(2)
"5.96"
```

Solution: use decimal numbers in C# or do pow()

articles:
- https://medium.com/@rtheunissen/accurate-numbers-in-php-b6954f6cd577
