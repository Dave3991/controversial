export interface Person {
  lastName?: string,
  firstName?: string
}

const person: Person = { firstName: "Joe", lastName: "Smith" };

let greet2 = (person: { firstName: string }) => `Hello ${person.firstName}`;
let greet3 = (person: any) =>`Hello ${person.firstName}`;
let greet4 = (person) =>`Hello ${person.firstName}`; // Error with --noImplictAny


greet2(27);    // => Error (as expected)
greet3(27);    //OK, but should be an Error
greet3(person) // Ok (as expected)

greet4(person) //OK (as expected)
greet4(27);    //OK, but should be an Error


//source: https://blog.codecentric.de/en/2021/01/rescript-compare-typescript-elm/
