```php
    /** @var PDO */
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    private function getPdo(): PDO
    {
        return $this->pdo;
    }

    public function update(): void
    {
        $pdo = $this->getPdo()
        $pdo-> ...
    }
```

vs

```php
    /** @var PDO */
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

     public function update(): void
    {
        $this->pdo-> ...
    }
```

more info:
https://martinfowler.com/bliki/SelfEncapsulation.html

disscussion:
https://www.reddit.com/r/PHP/comments/hx21tv/controversial_use_of_getter/